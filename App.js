import React from 'react';
import type {Node} from 'react';
import {Root} from './src';

const App: () => Node = () => {
  return <Root />;
};

export default App;
