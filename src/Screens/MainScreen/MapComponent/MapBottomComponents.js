import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';

export const MapBottomComponents = () => {
  const handleButton = () => {};

  return (
    <View style={s.w}>
      <TouchableOpacity activeOpacity={0.8} onPress={handleButton}>
        <View style={s.to}>
          <Text style={s.ttot}>Куда</Text>
        </View>
      </TouchableOpacity>
      <View style={s.tw}>

      </View>
      <TouchableOpacity activeOpacity={0.8} onPress={handleButton}>
        <View style={s.b}>
          <Text style={s.t}>Заказать</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const s = StyleSheet.create({
  w: {
    height: 200,
    borderRadius: 12,
    margin: 12,
    padding: 12,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  tw: {
    marginBottom: 12,
  },
  to: {
    borderRadius: 8,
    height: 48,
    padding: 12,
    backgroundColor: '#f9f9f9',
    // alignItems: 'center',
    justifyContent: 'center',
  },
  ttot: {
    color: '#000',
    fontWeight: '700',
  },
  b: {
    borderRadius: 8,
    height: 48,
    padding: 8,
    backgroundColor: '#f456d4',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
