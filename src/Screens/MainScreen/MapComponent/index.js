import React, {useEffect, useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import MapboxGL from '@rnmapbox/maps';
import {constants} from '../../../utils/constants';
import {MapButtons} from './MapButtons';
import Geolocation from 'react-native-geolocation-service';

MapboxGL.setWellKnownTileServer('Mapbox');
MapboxGL.setAccessToken(constants.MAPBOX_TOKEN);

const RequestPermision = async () => {
  const permission = await MapboxGL.requestAndroidLocationPermissions();
  return permission;
};

export const MapComponent = () => {
  const _map = useRef(null);
  const camera = useRef(null);
  let coords = [71.4541, 40.4854654];
  useEffect(() => {
    RequestPermision().then(r => console.log('test', r));
  }, []);

  const findMe = () => {
    Geolocation.getCurrentPosition(
      position => {
        coords = [position.coords.longitude, position.coords.latitude];
        camera.current?.setCamera({
          centerCoordinate: coords,
          zoomLevel: 16,
        });
      },
      error => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  };

  return (
    <View style={styles.page}>
      <View style={styles.container}>
        <MapboxGL.MapView ref={_map} style={styles.map} localizeLabels={true}>
          <MapboxGL.Camera
            ref={camera}
            zoomLevel={13}
            centerCoordinate={coords}
          />
          <MapboxGL.PointAnnotation id={'point'} coordinate={coords} />
        </MapboxGL.MapView>

        <MapButtons findMe={findMe} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    height: '100%',
    width: '100%',
  },
  map: {
    flex: 1,
  },
});
