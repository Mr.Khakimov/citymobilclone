import { Button, View, StyleSheet, TouchableOpacity, Text } from "react-native";
import {MapBottomComponents} from './MapBottomComponents';

export const MapButtons = ({findMe}) => {
  return (
    <View style={s.w}>
      <View />
      <View>
        {/*<Button title={'найди меня'}  />*/}
        <TouchableOpacity activeOpacity={0.8} onPress={findMe}>
          <View style={s.ww}>
            <Text style={s.t}>♦</Text>
          </View>
        </TouchableOpacity>

        <MapBottomComponents />
      </View>
    </View>
  );
};

const s = StyleSheet.create({
  w: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'space-between',
    // padding: 24,
  },
  ww: {
    width: 48,
    height: 48,
    borderRadius: 50,
    backgroundColor: '#fff',
    marginLeft: 'auto',
    marginRight: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  t: {
    fontSize: 16,
  }
});
