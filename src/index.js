import React from 'react';
import {MainScreen} from './Screens/MainScreen';

export const Root = () => {
  return <MainScreen />;
};
